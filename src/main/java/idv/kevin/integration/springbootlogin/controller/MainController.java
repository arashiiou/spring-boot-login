package idv.kevin.integration.springbootlogin.controller;

import idv.kevin.integration.springbootlogin.domain.SblUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        System.out.println("visit index ...");

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(principal instanceof UserDetails);
        if (principal instanceof UserDetails) {
            SblUser sblUser = (SblUser) principal;
            model.addAttribute("userName", sblUser.getUsername());
        } else {
            System.out.println("user null");
        }
        return "index";
    }

    @GetMapping(value = "/goLogin")
    public String goLogin() {
        return "loginPage";
    }
}
