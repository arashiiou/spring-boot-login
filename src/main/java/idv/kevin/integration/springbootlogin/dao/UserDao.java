package idv.kevin.integration.springbootlogin.dao;

import idv.kevin.integration.springbootlogin.domain.SblUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserDao extends JpaRepository<SblUser, Integer>, JpaSpecificationExecutor<SblUser> {

    public SblUser findByUserName(String userName);
}
