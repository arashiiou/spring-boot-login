package idv.kevin.integration.springbootlogin.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "role")
public class SblRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Column(name = "gmt_create",columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date gmtCreate;
    @Column(name = "gmt_modified",columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date gmtModified;

    @ManyToMany(mappedBy = "roles",fetch = FetchType.LAZY)
    private List<SblUser> users;

    @ManyToMany(mappedBy = "roles",fetch = FetchType.EAGER)
    private List<SblAuthority> authorities;


}
