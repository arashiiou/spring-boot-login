package idv.kevin.integration.springbootlogin.security;

import idv.kevin.integration.springbootlogin.dao.UserDao;
import idv.kevin.integration.springbootlogin.domain.SblUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class SblUserDetail implements UserDetailsService {

    @Autowired
    UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        SblUser sblUser = userDao.findByUserName(s);

        if(sblUser == null){
            throw new UsernameNotFoundException("user not exist");
        }
        return sblUser;
    }
}
