package idv.kevin.integration.springbootlogin;

import idv.kevin.integration.springbootlogin.dao.UserDao;
import idv.kevin.integration.springbootlogin.domain.SblUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootLoginApplicationTests.class)
public class Test11 {

    @Autowired
    private UserDao userDao;

    @Test
    public void run() {
        SblUser sblUser = userDao.findByUserName("xx");
        System.out.println(sblUser);
    }

    public void save(){
        SblUser sblUser = new SblUser();
        sblUser.setEmail("aaAAA@mm.com");
        sblUser.setFirstName("aa");
        sblUser.setLastName("AA");
        sblUser.setNickName("AaAa");
        sblUser.setPassword("123456");
        sblUser.setPhone("0987654321");
        sblUser.setUserName("a1a1a1");

        SblUser sblUser1 = userDao.save(sblUser);

        System.out.println(sblUser1);

    }

}
